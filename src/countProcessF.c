#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>

time_t now;
struct tm *tps_now;


int reste=0;
void stop (int sig);

int main(int argc, char* argv[])
{

	int decompte = atoi(argv[1]);
	signal(SIGINT,SIG_IGN);
	signal(SIGUSR1, stop); 
	sleep(1);
	int nbSec = decompte;

	tps_now=localtime(&now);

	while (1)
	{ 
		time_t timestamp = time( NULL );
		unsigned long seconds = difftime( timestamp, 0 );
		reste=seconds-nbSec;
	}
	return 0;
}

void stop (int sig){
int desc = open("resultat",O_CREAT|O_RDWR,0750);
int chiffreLue;
    if(read(desc,&chiffreLue,sizeof(chiffreLue)) == 0){ // Si rien est ecrit
        write(desc,&reste,sizeof(int)); // J'ecrit

    }
       else{

    int pos = lseek(desc,0,SEEK_SET); // revenir au debut pour ecraser
    write(desc,&reste,sizeof(int)); // ecraser la valeur

   }
exit(reste);

}

