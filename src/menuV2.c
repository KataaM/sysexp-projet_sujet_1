#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>


int affichageMenu(void)
{
        int choixMenu;
        printf("---Menu---\n\n");
        printf("1 - Réveil\n");
        printf("2 - Compte à rebours\n");
        printf("3 - Chronomètre\n");
        printf("4 - Statistiques\n");
        printf("5 - Monitoring\n");
        printf("6 - Exit\n");
        scanf("%d",&choixMenu);
        return choixMenu;
}

int main(void)
{
	int status;

	char *arguments[2];

	int pid = 0;
	pid = fork();

	if(pid==0){
		arguments[0]="countProcessP";
		arguments[1] = NULL;
		execv("countProcessP",arguments);
		}
	else{
		int choix;
		while(choix!=6)
		{
			choix = affichageMenu();
			switch(choix)
			{
				case 1 :
					printf("Exec du reveil...\n");
					system("xterm -e ./reveil 1 &");
					break;
				case 2 : 
					printf("Exec compte a rebours...\n");
					system("xterm -e ./compte_a_rebours 1 &");
					break;
				case 3 : 
					system("xterm -e ./chronometreOK 1 &");
					printf("Exec chrono... \n");
					 break;
				case 4 : 
					system("xterm -e ./zzz 1 &");
					printf("Exec Statistiques... \n");
					 break;
				case 5 : 
					system("xterm -e ./zzz 1 &");
					printf("Exec Monitoring... \n");
					 break;
				case 6 : 
					printf("Sortie du programme \n");
					exit(0);
					 break;
				default : printf("err");
					 break;
			}
		}
		sleep(1); 
		wait(&status);
	}
}
