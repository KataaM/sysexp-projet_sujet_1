
 #include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
 
void reveil(int sig){
    printf("Ca sonne lekip\n");
    wait(NULL);
    exit(0);
}

int main(void)
{
    int delaiH, delaiMin, pid;
    char h [10];
    char min [10];
    char * argrev[4];
 
    signal(SIGUSR1, reveil);
    printf("Entrez l'heure : ");
    scanf("%d", &delaiH);
    sprintf(h, "%d", delaiH);
    printf("Entrez les minutes : ");
    scanf("%d",&delaiMin);
    sprintf(min, "%d", delaiMin);    
    pid=fork();
    if(pid==0)
    {
        argrev[0]="reveilf";
        argrev[1]=h;
	argrev[2]=min;
        argrev[3]=NULL;
        execv("reveilf",argrev);
    }
    pause();
}
