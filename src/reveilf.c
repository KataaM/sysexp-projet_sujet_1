#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
time_t now;
struct tm *tps_now;

int main(int argc, char * argv[])
{
    int h, min;
    int hRev, minRev;
    h = atoi(argv[1]);
    min = atoi(argv[2]);
    while(1)
    {
	time(&now);
	tps_now=localtime(&now);
        if(h == tps_now->tm_hour & min == tps_now->tm_min){
		break;
	}
    }
    kill(getppid(),SIGUSR1);
    exit(0);
}
