#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <unistd.h> // pour getch (merci internet)
#include <termios.h>// pour getch (merci internet)

//gcc -Wall -o "test" "test.c" -lm 

int pid;

char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}//pour ne prendre que un character (super)



void continuer(){
	//printf("PARENT : on arrete les bêtises \n");
	//kill(pid,SIGKILL);
	//exit(3);
	
}
void arret(){
	printf("PARENT : Stop ! \n");
	kill(pid,SIGKILL);
	exit(0);
	
}

int main(void)
{
    
    int ch;
    char * argv[2];
    printf("Appuyer sur une touche pour lancer le chronometre \n \n");			
	ch = getch();
    pid=fork();

    if(pid == 0){
		
        argv[0]="chronometreFilsOK";
        argv[1]=NULL;
        execv("chronometreFilsOK",argv);
        
    }
    
    else{
		
		signal(SIGUSR1, continuer);
		signal(SIGUSR2, arret);
			
		while(1){
			
			printf("Appuyer sur 'A' pour marquer un tour et 'Z' pour quitter \n");			
			ch = getch();
			
			//printf("PARENT : ch= %d \n",ch);
			//wait(&status);	
			
			if (ch==97 || ch==65){ //97=a
				kill(pid,SIGUSR1);
				pause();
			}
			
			if (ch==122 || ch==90 ){ //122=z
				kill(pid,SIGUSR2);
				pause();
			}
		}
		
			
			/*wait(&status);
			printf("PARENT : Chrono arreté avec le status %d \n", status>>8);*/
		
    }
    return 0;
}
