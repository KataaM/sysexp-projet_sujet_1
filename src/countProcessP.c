#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>

time_t now;
struct tm *tps_now;




//Un processus fils (toujours présent) compte le temps d’exécution du logiciel depuis son lancement et le stock dans un fichier quand on quitte le programme

int pid = 0;

void controlC(int sig);

int main(int argc, char* argv[]){
	
	char *arguments[3];
	char touche;
	int status;
	 pid = fork();
	 
	time(&now);
	tps_now=localtime(&now);

	time_t timestamp = time( NULL );
	unsigned long seconds = difftime( timestamp, 0 );
    
	char str[2000];
	sprintf(str, "%ld", seconds);


	if(pid==0){
		arguments[0]="countProcessF";
		arguments[1]=str;
		arguments[2] = NULL;
		execv("countProcessF",arguments);
		}
	else{
		sleep(1); 
		signal(SIGINT,controlC);
		wait(&status);
		printf("Le programme a duré  %d s\n",status>>8);
	}
}

void controlC(int sig){

	kill(pid,SIGUSR1);
}

