#!/bin/sh

wish=true

while $wish
do

	DIALOG=${DIALOG=dialog}
	# La ligne dialogue sert à configurer le type de terminal utilisé : soit Xdialog soit dialog
	fichtemp=`tempfile 2>/dev/null` || fichtemp=/tmp/test$$
	trap "rm -f $fichtemp" 0 1 2 5 15
	# Les deux lignes ci dessus servent à créer un fichier temporaire à l'aide de l'utilitaire tempfile. Si ce dernier échoue, un fichier temporaire est configuré manuellement dans /tmp. Enfin, la seconde ligne met en place une routine d'interception : quand le script se termine (sur une erreur ou non), la commande trap permet de supprimer le fichier temporaire. Les nombres affichés sont les signaux qui seront intercepté par la commande.
	$DIALOG --clear --title "MENU PRINCIPAL" \
		 	--menu  "Bonjour, veuillez faire un choix :" 20 70 10\
				"../bin/reveil" "Ajouter un réveil" \
				"../bin/chronometreOK" "Lancer un chronomètre" \
				"../bin/compte_a_rebours" "Lancer un minuteur" \
				"../bin/zzz" "Affiche les statistiques de l'application" \
				"../bin/zzz" "Gestion globale de l'application" 2> $fichtemp
	valret=$?
	choix=`cat $fichtemp`
	case $valret in
		  0)   xterm -e ./$choix 1 & ;;
		  1) 	wish=false && echo "Fin du programme";;
		  255)  wish=false && echo "Fin du programme." && exit 0;;
	esac
done
